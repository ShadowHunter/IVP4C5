/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.domain;

import java.util.ArrayList;

/**
 *
 * @author Erik
 */
public class Schedule {
    
    private ArrayList employeeNrs = new ArrayList();
    private ArrayList employeeNames = new ArrayList();
    private ArrayList employeeSurNames = new ArrayList();
    private ArrayList dayParts = new ArrayList();
    
    public Schedule(ArrayList employeeNrs, ArrayList employeeNames, ArrayList employeeSurNames, ArrayList dayParts){
        this.employeeNrs = employeeNrs;
        this.employeeNames = employeeNames;
        this.employeeSurNames = employeeSurNames;
        this.dayParts = dayParts;
    }
    
    public ArrayList getEmployeeNrs(){
        return employeeNrs;
    }
    
    public ArrayList getEmployeeNames(){
        return employeeNames;
    }
    
    public ArrayList getEmployeeSurNames(){
        return employeeSurNames;
    }
    
    public ArrayList getDayParts(){
        return dayParts;
    }
    
    public void addEmployeeNrs(String employeeNr){
        employeeNrs.add(employeeNr);
    }
    
    public void addEmployeeNames(String employeeName){
        employeeNames.add(employeeName);
    }
    
    public void addEmployeeSurNames(String employeeSurName){
        employeeSurNames.add(employeeSurName);
    }
    
    public void addDayParts(String dayPart){
        dayParts.add(dayPart);
    }
}
