/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.domain;

/**
 *
 * @author Erik
 */
public class Attendance {
    
    private int attendanceID;
    private String dayTime;
    private String date;
    
    public Attendance(int attendanceID, String dayTime, String date){
        
        this.attendanceID = attendanceID;
        this.dayTime = dayTime;
        this.date = date;
    }
    
    public int getID(){
        
        return attendanceID;
    }
    
    public String getDayTime(){
        
        return dayTime;
    }
    
    public void setDayTime(String dayTime){
        
        this.dayTime = dayTime;
    }
    
    public String getDate(){
        
        return date;
    }
    
    public void setDate(String Date){
        
        this.date = date;
    }
    
    
     @Override
    public boolean equals(Object o)
    {
        boolean equal = false;
        
        if(o == this)
        {
            // Dezelfde instantie van de klasse, dus per definitie hetzelfde.
            equal = true;
        }
        else
        {
            if(o instanceof Attendance)
            {
                Attendance l = (Attendance)o;
                
                // Boek wordt geidentificeerd door ISBN, dus alleen hierop
                // controlleren is voldoend.
                equal = this.attendanceID == l.attendanceID;
            }
        }
        
        return equal;
    }
    
    @Override
    public int hashCode()
    {
        // Deze implementatie is gebaseerd op de best practice zoals beschreven
        // in Effective Java, 2nd edition, Joshua Bloch.
        
        // employeeNumber is uniek, dus voldoende als hashcode.
        return attendanceID;
    }
}
