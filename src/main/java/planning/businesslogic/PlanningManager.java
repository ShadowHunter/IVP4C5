/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.businesslogic;

import java.util.HashMap;
import planning.datastorage.AttendanceDAO;
import planning.datastorage.EmployeeDAO;
import planning.datastorage.PlanningDAO;
import planning.datastorage.ScheduleDAO;
import planning.domain.Attendance;
import planning.domain.Employee;
import planning.domain.Planning;
import planning.domain.Schedule;

/**
 *
 * @author Erik
 */
public class PlanningManager {
    
    private final HashMap <Integer, Employee> employees;
    private final HashMap <Integer, Attendance> attendant;
    
    public PlanningManager(){
        
        employees = new HashMap();
        attendant = new HashMap();
    }
    
    public Employee findEmployee(int employeeNr){
        
        Employee employee = employees.get(employeeNr);
        
        if(employee == null){
            //Employee may not have been loaded from the database yet. Try to do so
            EmployeeDAO employeeDAO = new EmployeeDAO();
            employee = employeeDAO.findEmployee(employeeNr);
            
        }
        
        return employee;
    }
    
    public Planning findEmployeePlanning(int employeeNr){
        
        Planning planning;
        PlanningDAO planningdao = new PlanningDAO();
        planning = planningdao.findEmployeePlanning(employeeNr);
        
        return planning;
    }
    
    public void addEmployee(String EmployeeName, String EmployeeLastname, String EmployeeType)
    {
    EmployeeDAO employeeDAO = new EmployeeDAO();
    employeeDAO.addEmployee(EmployeeName, EmployeeLastname, EmployeeType);
    }
    
    public void removeEmployee(int employeeNr)
    {
        boolean result = true;
            //Employee employee = employees.get(employeeNr);
            //int employeeNumber = employee.getEmployeeNumber();
                    
            // Let the member remove itself as a domain object. Do this before
            // removing from the database. In case something goes wrong, we
            // still have the data in the database. If first the member was
            // removed from the database, we might end up in an inconsistent
            // state.
            //result = employee.remove();
            
            if(result)
            {
                // Let the member remove itself from the database.
                EmployeeDAO employeeDAO = new EmployeeDAO();
                employeeDAO.removeEmployee(employeeNr);
                
                // In case something goes wrong here, we need to roll back.
                // But that's too much for this version of the POC.
            }
            
            // Finally, remove the member from the map in this manager.
            employees.remove(employeeNr);
        
        
        //return employee;
    }
    
    public boolean addPlanning(int employeeNr, String addDate, String addDaytime){
        
        boolean result;
        
        PlanningDAO planningdao = new PlanningDAO();
        result = planningdao.addPlanning(employeeNr, addDate, addDaytime);
        
        return result;
    }
    
    public boolean deletePlanning(int employeeNr, String date, String dayTime) {
        
        boolean result;
        
        PlanningDAO planningdao = new PlanningDAO();
        result = planningdao.deletePlanning(employeeNr, date , dayTime);
        
        return result;
    }
    
    public boolean reportAttendance(String date){
        
        boolean result;
        
        AttendanceDAO attendancedao = new AttendanceDAO();
        result = attendancedao.reportAttendance(date);
        
        return result;
    }    
    
    public Attendance checkAttendance(int attendanceID){
        
        Attendance attendance = attendant.get(attendanceID);
        
        if(attendance == null){
            //Employee may not have been loaded from the database yet. Try to do so
            AttendanceDAO attendanceDAO = new AttendanceDAO();
            attendance = attendanceDAO.checkAttendance(attendanceID);            
        }        
        return attendance;
    }

    public Schedule fillBedieningTable(String dateFromField) {
        
        Schedule schedule;
        ScheduleDAO scheduledao = new ScheduleDAO();
        schedule = scheduledao.fillBedieningTable(dateFromField);
        
        return schedule;
    }

    public Schedule fillKeukenTable(String dateFromField) {
        
        Schedule schedule;
        ScheduleDAO scheduledao = new ScheduleDAO();
        schedule = scheduledao.fillKeukenTable(dateFromField);
        
        return schedule;
    }

    public boolean plannerLogin(int employeeNr, String password) {
        boolean result;
        
        EmployeeDAO employeeDAO = new EmployeeDAO();
        result = employeeDAO.plannerLogin(employeeNr, password);
        
        return result;
    }

    public boolean employeeLogin(int employeeNr, String password) {
        boolean result;
        
        EmployeeDAO employeeDAO = new EmployeeDAO();
        result = employeeDAO.employeeLogin(employeeNr, password);
        
        return result;
    }

    public int countEmployees(int employeeNr) {
        int orders;
        
        EmployeeDAO employeeDAO = new EmployeeDAO();
        orders = employeeDAO.countEmployees(employeeNr);
        
        return orders;
    }
}
