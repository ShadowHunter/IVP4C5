/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.main;

import java.io.IOException;
import planning.presentation.PlannerUI;
/**
 *
 * @author Erik
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        PlannerUI ui = new PlannerUI();
        ui.setVisible(true);
    }   
}
