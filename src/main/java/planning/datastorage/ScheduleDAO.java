/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import planning.domain.Schedule;
/**
 *
 * @author Erik
 */
public class ScheduleDAO {

    private ArrayList employeeNrs = new ArrayList();
    private ArrayList employeeNames = new ArrayList();
    private ArrayList employeeSurNames = new ArrayList();
    private ArrayList dayParts = new ArrayList();
    
    public ScheduleDAO(){}
    
    public Schedule fillBedieningTable(String dateFromField) {
        
        Schedule schedule = new Schedule(employeeNrs, employeeNames, employeeSurNames, dayParts);
        
        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection())
        {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                "SELECT employee.EmployeeNr, EmployeeName, EmployeeLastname, Daytime FROM employee JOIN schedule ON schedule.EmployeeNr = employee.EmployeeNr JOIN date ON date.ID = schedule.ID "
                        + "WHERE Date ='" + dateFromField + "' && AccountType = 'bediening' ORDER BY Daytime ASC;");

            if(resultset != null)
            {
                try
                {
                    // The gerechtNumber for a gerecht is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while(resultset.next())
                    {
                        
                        int employeeNrFromDb = resultset.getInt("EmployeeNr");
                        String employeeNameFromDb = resultset.getString("EmployeeName");
                        String employeeSurNameFromDb = resultset.getString("EmployeeLastname");
                        String dayPartFromDb = resultset.getString("Daytime");
                        
                        employeeNrs.add(employeeNrFromDb);
                        employeeNames.add(employeeNameFromDb);
                        employeeSurNames.add(employeeSurNameFromDb);
                        dayParts.add(dayPartFromDb);
                        
                    }
                }
                catch(SQLException e)
                {
                    System.out.println(e);
                    
                }
            }
            
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }
        
        return schedule;
    }

    public Schedule fillKeukenTable(String dateFromField) {
        Schedule schedule = new Schedule(employeeNrs, employeeNames, employeeSurNames, dayParts);
        
        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection())
        {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                "SELECT employee.EmployeeNr, EmployeeName, EmployeeLastname, Daytime FROM employee JOIN schedule ON schedule.EmployeeNr = employee.EmployeeNr JOIN date ON date.ID = schedule.ID "
                        + "WHERE Date ='" + dateFromField + "' && AccountType = 'keuken' ORDER BY Daytime ASC;");

            if(resultset != null)
            {
                try
                {
                    // The gerechtNumber for a gerecht is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while(resultset.next())
                    {
                        
                        int employeeNrFromDb = resultset.getInt("EmployeeNr");
                        String employeeNameFromDb = resultset.getString("EmployeeName");
                        String employeeSurNameFromDb = resultset.getString("EmployeeLastname");
                        String dayPartFromDb = resultset.getString("Daytime");
                        
                        employeeNrs.add(employeeNrFromDb);
                        employeeNames.add(employeeNameFromDb);
                        employeeSurNames.add(employeeSurNameFromDb);
                        dayParts.add(dayPartFromDb);
                        
                    }
                }
                catch(SQLException e)
                {
                    System.out.println(e);
                    
                }
            }
            
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }
        
        return schedule;
    }
    
}
