/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import planning.domain.Planning;
/**
 *
 * @author Erik
 */
public class PlanningDAO {
    
    private ArrayList dates = new ArrayList();
    private ArrayList daytimes = new ArrayList();
    
    public PlanningDAO(){}
    
    public Planning findEmployeePlanning(int employeeNr){       
        
        Planning planning = new Planning(dates,daytimes);
        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection())
        {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                "SELECT Daytime,Date FROM schedule, date WHERE EmployeeNr = " + employeeNr + " && date.ID = schedule.ID ORDER BY Date;");

            if(resultset != null)
            {
                try
                {
                    // The gerechtNumber for a gerecht is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while(resultset.next())
                    {
                        
                        String dateFromDb = resultset.getString("Date");
                        String daytimeFromDb = resultset.getString("Daytime");
                        
                        dates.add(dateFromDb);
                        daytimes.add(daytimeFromDb);
                        
                        //System.out.println(planning.getDates());
                    }
                }
                catch(SQLException e)
                {
                    System.out.println(e);
                    
                }
            }
            // else an error occurred leave 'gerecht' to null.
            
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }
        return planning;
    }
    
    public boolean addPlanning(int employeeNr, String addDate, String addDaytime){
    
        boolean result = false;
         // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection())
        {
            // If a connection was successfully setup, execute the statements.
            
            int IDFromDb =0;
            
            //check for the id
            ResultSet resultAttendance = connection.executeSQLSelectStatement(
            "SELECT ID FROM date WHERE Daytime ='"+ addDaytime + "' && Date = '" + addDate + "';");
            
            try{
                //if not found: make the id
                if (resultAttendance.next() == false){
            
                    connection.executeSQLInsertStatement(
                    "INSERT INTO date(Daytime, Date) VALUES('" + addDaytime + "','" + addDate +"');");
                }
            }
            catch(SQLException e){
                System.out.println(e);
            }
            
            //check for the id again
            resultAttendance = connection.executeSQLSelectStatement(
            "SELECT ID FROM date WHERE Daytime ='"+ addDaytime + "' && Date = '" + addDate + "';");
            
            if(resultAttendance != null){
            //get the id
                try{ 
                    if(resultAttendance.next()){
                        IDFromDb = resultAttendance.getInt("ID");                                               
                    }    
                }
                catch(SQLException e){
                    System.out.println(e);
                }
            }
            
            result = connection.executeSQLInsertStatement(
            "INSERT INTO schedule(EmployeeNr, ID) VALUES('" + employeeNr + "','" + IDFromDb + "')");
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }
        return result;
    }

    public boolean deletePlanning(int employeeNr, String date, String dayTime) {
        
        boolean result = false;
        //First open a database connection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection()){
            
            int IDFromDb =0;
            
            ResultSet resultAttendance = connection.executeSQLSelectStatement(
            "SELECT ID FROM date WHERE Daytime ='"+ dayTime + "' && Date = '" + date + "';");
            
            if(resultAttendance != null){
            //get the id
                try{ 
                    if(resultAttendance.next()){
                        IDFromDb = resultAttendance.getInt("ID");                                               
                    }    
                }
                catch(SQLException e){
                    System.out.println(e);
                }
            }
            
            result = connection.executeSQLDeleteStatement(
            "DELETE FROM schedule WHERE EmployeeNr = " + employeeNr + " && ID = " + IDFromDb + ";");
            
            connection.closeConnection();
        }        
        return result;
    }
}
