/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.datastorage;

import planning.domain.Attendance;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author Erik
 */
public class AttendanceDAO {
    
    public AttendanceDAO(){
        // Nothing to be initialized. This is a stateless class. Constructor
        // has been added to explicitely make this clear.
    }
    
    /**
     * 
     * @param employeeNr identifies the member to be loaded from the database
     * 
     * @return the Employee object to be found. In case employee could not be found,
        null is returned.
     */
    
    public Attendance checkAttendance(int EmployeeNr)
    {
        Attendance attendance = null;
        
        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection())
        {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                "SELECT * FROM schedule WHERE EmployeeNr = " + EmployeeNr + ";");

            if(resultset != null)
            {
                try
                {
                    // The gerechtNumber for a gerecht is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    if(resultset.next())
                    {
                        int attendanceIDFromDb = resultset.getInt("EmployeeNr");
                        String idFromDb = resultset.getString("ID");
                        String attendanceCheckFromDb = resultset.getString("AttendanceCheck");

                        attendance = new Attendance(
                            attendanceIDFromDb,
                            idFromDb,
                            attendanceCheckFromDb);
                    }
                }
                catch(SQLException e)
                {
                    System.out.println(e);
                    attendance = null;
                }
            }
            // else an error occurred leave 'gerecht' to null.
            
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }
        
        return attendance;
    }
    
    public boolean reportAttendance(String date)
    {
        
        boolean result = false;
        
        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection())
        {
            // If a connection was successfully setup, execute the SELECT statement.
            
            result = connection.executeSQLInsertStatement(
            "UPDATE INTO schedule(AttendanceCheck) VALUES ('" + date + "')");  
            
           
            }
            // else an error occurred leave 'member' to null.
            
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
      return result;
    }
        
    public boolean showEmployee()
    {        
        boolean result = false;
        
       
      return result;
    }
    
}
