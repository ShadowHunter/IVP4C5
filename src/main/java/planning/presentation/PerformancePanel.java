/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import planning.businesslogic.PlanningManager;
import planning.domain.Employee;

/**
 *
 * @author Erik
 */
class PerformancePanel extends JPanel {

    private JTextField employeeNrField;
    private JButton searchEmployeeButton;
    
    //The PlanningManager to delegate the real work to.
    private final PlanningManager manager;
    
    private Employee currentEmployee;
    private JTextArea employeeResultArea;
    
    public PerformancePanel() {
        
        manager = new PlanningManager();
        currentEmployee = null;
        
        //borderlayout
        setLayout(new BorderLayout(5,5));
        
        //setup of north area
        JPanel northPanel = createNorthPanel();
        
        //setup of center area
        JPanel centerPanel = createCenterPanel();
        
        add(northPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
        
        //add ActionListener
        searchEmployeeButton.addActionListener ((ActionEvent e) -> {
            //check whether the field is empty
            //if no:
            if(!(employeeNrField.getText().isEmpty())){
                //go find the employee
                int employeeNr = Integer.parseInt(employeeNrField.getText());
                doFindEmployee(employeeNr);
            }
            //if yes:
            else{
                //let the user know he/she needs to fill in a number
                JOptionPane.showMessageDialog(null,"Vul een nummer in");
            }
        });
    }
    
    private JPanel createNorthPanel(){
        JPanel employeeNrPanel = new JPanel();
        employeeNrPanel.setLayout(new BoxLayout(employeeNrPanel, BoxLayout.X_AXIS));
      
        employeeNrPanel.add(new JLabel("Werknemer nummer"));
      
        employeeNrField = new JTextField(10);
        employeeNrPanel.add(employeeNrField);
      
        searchEmployeeButton = new JButton("Zoek");
        employeeNrPanel.add(searchEmployeeButton);
        
        return employeeNrPanel;
    }
    
    private JPanel createCenterPanel(){
        JPanel employeeResultPanel = new JPanel();
        employeeResultPanel.setLayout(new GridLayout());
      
        employeeResultArea =  new JTextArea();
        employeeResultPanel.add(employeeResultArea);
        employeeResultArea.setText("");
        employeeResultArea.setEditable(false);
        
        return employeeResultPanel;
    }
    
    private void doFindEmployee(int employeeNr) {
        
        String employeeInfo = "";
        currentEmployee = manager.findEmployee(employeeNr);
        int orderCount = manager.countEmployees(employeeNr);
       
        if(currentEmployee == null){
            
            JOptionPane.showMessageDialog(null,"Werknemer is niet gevonden");
        }
        
        if(currentEmployee != null){
            
            //add employeeInfo
            employeeInfo = "Personeelsnummer: " + currentEmployee.getEmployeeNumber() +
            "\n" + "\n" + 
            "Voornaam: " + currentEmployee.getForname() + 
            "\n" + "\n" +
            "Achternaam: " + currentEmployee.getSurname() +
            "\n" + "\n" +
            "Personeelstype: " + currentEmployee.getAccountType() +
            "\n" + "\n" +
            "Contracturen per week: " + currentEmployee.getContractHour() +
            "\n" + "\n" +        
            "Totaal aantal orders gebracht: " + orderCount;
        }
        employeeResultArea.setText(employeeInfo);
    }            
}
