/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author Erik
 */
public class AttendanceMain extends JPanel {
    
    private JButton addAttedanceCheckButton;
    private JButton addAttedanceReportButton;
    JPanel contentPane;
    
    //Creates new form PlanningUI
    public AttendanceMain(){
        setLayout( new BorderLayout());        
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,2));
        
        //rij 1
        contentPane.add(new JLabel("Aanwezigheid melden voor de dag"));
        addAttedanceCheckButton = new JButton("Meld aanwezigheid");
        contentPane.add(addAttedanceCheckButton);        
        
        contentPane.add(new JLabel(""));contentPane.add(new JLabel(""));
        
        //rij2
        contentPane.add(new JLabel("Aanwezigheid checken voor het personeel"));
        addAttedanceReportButton = new JButton("Check aanwezigheid");
        contentPane.add(addAttedanceReportButton);
        
        //Event handlers
        
        //add new frame for adding personnel
        addAttedanceCheckButton.addActionListener ((ActionEvent e) -> {
            JFrame AttedanceCheckFrame = new JFrame("Voeg personeel toe");
            AttedanceCheckFrame.setSize(600,200);
            AttedanceCheckFrame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
            AttedanceCheckFrame.setContentPane(new AttendanceReportPanel());
            AttedanceCheckFrame.setVisible(true);
            AttedanceCheckFrame.setResizable(false);
            
            //center screen
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            AttedanceCheckFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        });
        
        //add new frame for deleting personnel
        addAttedanceReportButton.addActionListener((ActionEvent e) ->{
            JFrame AttedanceReportFrame = new JFrame("Verwijder personeel");
            AttedanceReportFrame.setSize(600,200);
            AttedanceReportFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            AttedanceReportFrame.setContentPane(new AttendanceCheckPanel());
            AttedanceReportFrame.setVisible(true);
            AttedanceReportFrame.setResizable(false);
            
            //center screen
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            AttedanceReportFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        });
        
        
        //set the size of the window
        setSize(600,200);
        
        //center screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        
        
        add(contentPane, BorderLayout.CENTER);
        
    }
    
    
}
