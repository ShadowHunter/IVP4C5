/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import java.awt.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import planning.businesslogic.PlanningManager;
import planning.domain.Employee;
import planning.domain.Planning;

/**
 *
 * @author Erik
 */
class EmployeePlanningPanel extends Container {

    //The PlanningManager to delegate the real work to.
    private final PlanningManager manager;
    
    private Employee currentEmployee;
    private Planning planning;
    
    private JTextArea employeeResultArea;
    
    //necessary things for table
    String[] columns = {"Dag","Datum","Dagdeel","Totaal aantal uur"};
    DefaultTableModel model = new DefaultTableModel(columns,0);
    JTable table = new JTable(model);
    
    public EmployeePlanningPanel(int employeeNr) {
        
        manager = new PlanningManager();
        
        //borderlayout
        setLayout(new BorderLayout(5,5));
        
        //setup of west area
        JPanel employeeInfoPanel = createEmployeeInfoPanel();
        
        //setup of center area
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new GridLayout());
        tablePanel.add(table);
        
        //add scrollpane
        JScrollPane scrollPane = new JScrollPane(table);
        tablePanel.add(scrollPane);
        
        add(employeeInfoPanel, BorderLayout.WEST);
        add(tablePanel, BorderLayout.CENTER);
        
        doFindEmployee(employeeNr);
    }
    
    private JPanel createEmployeeInfoPanel() {

        JPanel employeeInfoPanel = new JPanel();
        employeeInfoPanel.setLayout(new GridLayout());
      
        employeeResultArea =  new JTextArea();
        employeeInfoPanel.add(employeeResultArea);
        employeeResultArea.setText("");
        employeeResultArea.setEditable(false);
       
        return employeeInfoPanel;
    }
    
    private void doFindEmployee(int employeeNr) {
        
        String employeeInfo = "";
        currentEmployee = manager.findEmployee(employeeNr);
        planning = manager.findEmployeePlanning(employeeNr);
       
        if(currentEmployee == null){
            
            JOptionPane.showMessageDialog(null,"Werknemer is niet gevonden");
        }
        
        if(currentEmployee != null){
            
            //add employeeInfo
            employeeInfo = "Personeelsnummer: " + currentEmployee.getEmployeeNumber() +
            "\n" + "\n" + 
            "Voornaam: " + currentEmployee.getForname() + 
            "\n" + "\n" +
            "Achternaam: " + currentEmployee.getSurname() +
            "\n" + "\n" +
            "Personeelstype: " + currentEmployee.getAccountType() +
            "\n" + "\n" +
            "Contracturen per week: " + currentEmployee.getContractHour();
            
            //delete all current rows
            while(model.getRowCount() >0){
                model.removeRow(0);
            }
            
            LocalDate today = LocalDate.now();
        
            //go backward to get monday
            LocalDate monday = today;
            while(monday.getDayOfWeek()!= DayOfWeek.MONDAY){
                monday = monday.minusDays(1);
            }
        
            LocalDate tuesday = monday.plusDays(1);
            LocalDate wednesday = tuesday.plusDays(1);
            LocalDate thursday = wednesday.plusDays(1);
            LocalDate friday = thursday.plusDays(1);
            LocalDate saturday = friday.plusDays(1);
            LocalDate sunday = saturday.plusDays(1);
            
            //add the new rows
            for(int i = 0; i != planning.getDates().size(); i++){
                               
                if(planning.getDates().get(i).equals(monday.toString())){                
                    model.addRow(new Object[]{"Maandag", planning.getDates().get(i),planning.getDaytimes().get(i), "4"});               
                }
                
                else if(planning.getDates().get(i).equals(tuesday.toString())){                
                    model.addRow(new Object[]{"Dinsdag",planning.getDates().get(i),planning.getDaytimes().get(i), "4"});               
                }
                
                else if(planning.getDates().get(i).equals(wednesday.toString())){                
                    model.addRow(new Object[]{"Woensdag",planning.getDates().get(i),planning.getDaytimes().get(i), "4"});               
                }
                
                else if(planning.getDates().get(i).equals(thursday.toString())){                
                    model.addRow(new Object[]{"Donderdag",planning.getDates().get(i),planning.getDaytimes().get(i), "4"});               
                }
                
                else if(planning.getDates().get(i).equals(friday.toString())){                
                    model.addRow(new Object[]{"Vrijdag",planning.getDates().get(i),planning.getDaytimes().get(i), "4"});               
                }
                
                else if(planning.getDates().get(i).equals(saturday.toString())){                
                    model.addRow(new Object[]{"Zaterdag",planning.getDates().get(i),planning.getDaytimes().get(i), "4"});               
                }
                
                else if(planning.getDates().get(i).equals(sunday.toString())){                
                    model.addRow(new Object[]{"Zondag",planning.getDates().get(i),planning.getDaytimes().get(i), "4"});               
                }
            }
        }       
        employeeResultArea.setText(employeeInfo);
    }
    
}
