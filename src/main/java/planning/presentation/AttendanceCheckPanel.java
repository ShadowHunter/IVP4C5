/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import planning.businesslogic.PlanningManager;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import planning.domain.Attendance;
/**
 *
 * @author Erik
 */
public class AttendanceCheckPanel extends JPanel {
    JPanel contentPane;
    private JButton searchButton;
    private JTextField attendanceNumber, attendanceTime, attendanceDate;
    
    private final PlanningManager manager;
    private Attendance attendant;
    
    //Creates new form PlanningUI
    public AttendanceCheckPanel(){
        manager = new PlanningManager();
        attendant = null;
        setLayout( new BorderLayout());        
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,2));
        
        //rij 1
        attendanceNumber= new JTextField("", 30);
        searchButton = new JButton("zoek");
        SearchFunction bf2 = new SearchFunction();
        searchButton.addActionListener( bf2 );        
        contentPane.add(attendanceNumber); 
        contentPane.add(searchButton);
                
        //rij 2
        attendanceTime= new JTextField("", 30);
        attendanceTime.setEditable(false);
        contentPane.add(attendanceTime); 
        contentPane.add( new JLabel( " " ));  
        
        //rij 3
        attendanceDate= new JTextField("", 30);
        attendanceDate.setEditable(false);
        contentPane.add(attendanceDate); 
        contentPane.add( new JLabel( " " ));
        
        
      
        //set the size of the window
        setSize(600,200);
        
        //center screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        
        
        add(contentPane, BorderLayout.CENTER);
        
    }
    
    class SearchFunction implements ActionListener
        {
            public void actionPerformed(ActionEvent e)
            {
                if((attendanceNumber.getText().isEmpty())){
                JOptionPane.showMessageDialog(null,"Vul nummer in");
                }
                else{
                int attendanceID = Integer.parseInt(attendanceNumber.getText());
                doCheckAttendance(attendanceID);     
                }
            }
        } 
    
    private void doCheckAttendance(int attendanceID) {
        
        attendant = manager.checkAttendance(attendanceID);
       
        if(attendant == null){
            
            JOptionPane.showMessageDialog(null,"aanwezigheid is niet gevonden");
        }
        
        if(attendant != null){
            
            //personnelNr.setText(currentEmployee.getEmployeeNumber());
            attendanceTime.setText(attendant.getDayTime());
            attendanceDate.setText(attendant.getDate());
            
        }
        }
    
}
