/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import planning.businesslogic.PlanningManager;
import planning.domain.Schedule;

/**
 *
 * @author Erik
 */
class ShowSchedulePanel extends JPanel {

    private JTextField dateField;
    private JButton searchButton;
    
    //get current date
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    
    //necessary things for uppertable
    String[] columns = {"Personeelsnummer","Voornaam","Achternaam","Dagdeel","Werktijd"};
    DefaultTableModel upperModel = new DefaultTableModel(columns,0);
    JTable upperTable = new JTable(upperModel);
    //private final TableColumnModel upperTcm = upperTable.getColumnModel();
    
    //necessary things for lowertable
    DefaultTableModel lowerModel = new DefaultTableModel(columns,0);
    JTable lowerTable = new JTable(lowerModel);
    //private final TableColumnModel lowerTcm = lowerTable.getColumnModel();
    
    //The PlanningManager to delegate the real work to.
    private final PlanningManager manager;
    
    private Schedule schedule;
    
    public ShowSchedulePanel() {
        manager = new PlanningManager();
        
        setLayout(new BorderLayout());
        
        //setup north area
        JPanel upperPanel = new JPanel();
        upperPanel.add(new JLabel("Voer een datum in volgens: yyyy-mm-dd"));
        dateField = new JTextField(10);
        upperPanel.add(dateField);
        
        searchButton = new JButton("Zoek");
        upperPanel.add(searchButton);
        
        //setup center area
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout());
        centerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),"Bediening",TitledBorder.CENTER, TitledBorder.TOP));
        
        //add first of 2 total tables
        centerPanel.add(upperTable);
        JScrollPane upperScrollPane = new JScrollPane(upperTable);
        centerPanel.add(upperScrollPane);
        
        //setup south area 
        JPanel lowerPanel = new JPanel();
        lowerPanel.setLayout(new GridLayout());
        lowerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),"Keuken",TitledBorder.CENTER, TitledBorder.TOP));
        
        //add second table
        lowerPanel.add(lowerTable);
        JScrollPane lowerScrollPane = new JScrollPane(lowerTable);
        lowerPanel.add(lowerScrollPane);
        
        add(upperPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
        add(lowerPanel, BorderLayout.SOUTH);
        
        //Event handlers
        searchButton.addActionListener((ActionEvent e) -> {
            
            //get the date from the input field
            String dateFromField = dateField.getText();
            
            if(dateFromField.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")){
                //get the values from the database for the upper table
                schedule = manager.fillBedieningTable(dateFromField);              
                
                //delete all current rows
                while(upperModel.getRowCount() >0){
                upperModel.removeRow(0);
                }
            
                //add the new rows
                for(int i = 0; i != schedule.getEmployeeNames().size(); i++){
                
                    if(schedule.getDayParts().get(i).equals("ochtend")){                
                        upperModel.addRow(new Object[]{schedule.getEmployeeNrs().get(i),schedule.getEmployeeNames().get(i),schedule.getEmployeeSurNames().get(i),schedule.getDayParts().get(i),"9.00 - 13.00 uur"});               
                    }
                
                    else if(schedule.getDayParts().get(i).equals("middag")){                
                        upperModel.addRow(new Object[]{schedule.getEmployeeNrs().get(i),schedule.getEmployeeNames().get(i),schedule.getEmployeeSurNames().get(i),schedule.getDayParts().get(i),"13.00 - 17.00 uur"});               
                    }
                
                    else if(schedule.getDayParts().get(i).equals("avond")){                
                        upperModel.addRow(new Object[]{schedule.getEmployeeNrs().get(i),schedule.getEmployeeNames().get(i),schedule.getEmployeeSurNames().get(i),schedule.getDayParts().get(i),"17.00 - 21.00 uur"});               
                    }
                }
                
                schedule = manager.fillKeukenTable(dateFromField);
                
                //delete all current rows
                while(lowerModel.getRowCount() > 0){
                    lowerModel.removeRow(0);
                }
                
                //add the new rows
                for(int i = 0; i != schedule.getEmployeeNames().size(); i++){
                
                    if(schedule.getDayParts().get(i).equals("ochtend")){                
                        lowerModel.addRow(new Object[]{schedule.getEmployeeNrs().get(i),schedule.getEmployeeNames().get(i),schedule.getEmployeeSurNames().get(i),schedule.getDayParts().get(i),"9.00 - 13.00 uur"});               
                    }
                
                    else if(schedule.getDayParts().get(i).equals("middag")){                
                       lowerModel.addRow(new Object[]{schedule.getEmployeeNrs().get(i),schedule.getEmployeeNames().get(i),schedule.getEmployeeSurNames().get(i),schedule.getDayParts().get(i),"13.00 - 17.00 uur"});               
                    }
                
                    else if(schedule.getDayParts().get(i).equals("avond")){                
                        lowerModel.addRow(new Object[]{schedule.getEmployeeNrs().get(i),schedule.getEmployeeNames().get(i),schedule.getEmployeeSurNames().get(i),schedule.getDayParts().get(i),"17.00 - 21.00 uur"});               
                    }
                }
            }
            
            else{
                JOptionPane.showMessageDialog(null,"Voer een datum in volgens: yyyy-mm-dd");
                //clear the (already) made entry
                dateField.setText("");
            }
        });
    }
    
}
