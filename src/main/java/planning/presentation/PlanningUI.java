/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author Erik
 */
public class PlanningUI extends JPanel {
    
    private final JButton addPersonnelButton;
    //private final JButton addAttendanceButton;
    private final JButton addPlanningButton;
    private final JButton showScheduleButton;
    private final JButton showHoursButton;
    private final JButton performanceButton;
    
    public PlanningUI(){

        setLayout(new GridLayout(9,2));
        
        //row 1
        add(new JLabel("Wijzig het personeelsbestand"));
        addPersonnelButton = new JButton("Personeel");
        add(addPersonnelButton);
        
        //row 2, just a fill up
        add(new JLabel(""));
        add(new JLabel(""));
        
        /*werkt niet
        //row 3
        add(new JLabel("Aanwezigheid opvragen per werknemer"));        
        addAttendanceButton = new JButton("Aanwezigheid checken");
        add(addAttendanceButton);
        
        //row 4, just a fill up
        add(new JLabel(""));
        add(new JLabel(""));
        */
        //row 5
        add(new JLabel("Toevoeging en wijziging planning per werknemer"));        
        addPlanningButton = new JButton("Maak werknemer planning");
        add(addPlanningButton);
        
        //row 6, just a fill up
        add(new JLabel(""));
        add(new JLabel(""));
        
        //row 7
        add(new JLabel("Overzicht ingeroosterd personeel"));        
        showScheduleButton = new JButton("Rooster overzicht");
        add(showScheduleButton);
        
        //row 8, just a fill up
        add(new JLabel(""));
        add(new JLabel(""));
        
        //row 9 
        add(new JLabel("Urenoverzicht per medewerker"));
        showHoursButton = new JButton("Urenoverzicht");
        add(showHoursButton);
        
        //row 10, just a fill up
        add(new JLabel(""));
        add(new JLabel(""));
        
        //row 11
        add(new JLabel("Prestatieoverzicht"));
        performanceButton = new JButton("Prestatieoverzicht");
        add(performanceButton);
        
        //Event handlers
        
        //add new frame for adding personnel
        addPersonnelButton.addActionListener ((ActionEvent e) -> {
            JFrame personnelFrame = new JFrame("Personeel");
            personnelFrame.setSize(600,200);
            personnelFrame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
            personnelFrame.setContentPane(new PersonnelMain());
            personnelFrame.setVisible(true);
            personnelFrame.setResizable(false);
            
            //center screen
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            personnelFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        });
        
        //add new frame for adding and changing a schedule per employee
        
        /* addAttendanceButton.addActionListener((ActionEvent e) ->{
            JFrame AttendanceFrame = new JFrame("Aanwezigheid controleren werknemers");
            AttendanceFrame.setSize(600,200);
            AttendanceFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            AttendanceFrame.setContentPane(new AttendanceCheckPanel());
            AttendanceFrame.setVisible(true); 
            AttendanceFrame.setResizable(false);
            
            //center screen
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            AttendanceFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        }); 
           */
        
        //add new frame for adding and changing a schedule per employee
        addPlanningButton.addActionListener((ActionEvent e) ->{
            JFrame addPlanningFrame = new JFrame("Toevoeging en wijziging planning per werknemer");
            addPlanningFrame.setSize(800,400);
            addPlanningFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            addPlanningFrame.setContentPane(new PlanningPanel());
            addPlanningFrame.setVisible(true);
            addPlanningFrame.setResizable(false);
            
            //center screen
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            addPlanningFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        });
        
        //add new frame for showing the schedule
        showScheduleButton.addActionListener((ActionEvent e) ->{
            JFrame showScheduleFrame = new JFrame("Rooster overzicht");
            showScheduleFrame.setSize(1600,950);
            showScheduleFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            showScheduleFrame.setContentPane(new ShowSchedulePanel());
            showScheduleFrame.setVisible(true); 
            showScheduleFrame.setResizable(false);
            
            //set screen
            showScheduleFrame.setLocation(150, 50);
        }); 
        
        showHoursButton.addActionListener((ActionEvent e) ->{
            JFrame showHoursFrame = new JFrame("Urenoverzicht");
            showHoursFrame.setSize(800,400);
            showHoursFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            showHoursFrame.setContentPane(new ShowHoursPanel());
            showHoursFrame.setVisible(true);
            showHoursFrame.setResizable(false);
            
            //center screen
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            showHoursFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        });
        
        performanceButton.addActionListener((ActionEvent e) ->{
            JFrame performanceFrame = new JFrame("Prestatieoverzicht");
            performanceFrame.setSize(600,300);
            performanceFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            performanceFrame.setContentPane(new PerformancePanel());
            performanceFrame.setVisible(true);
            performanceFrame.setResizable(false);
            
            //center screen
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            performanceFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        });
    }
}
