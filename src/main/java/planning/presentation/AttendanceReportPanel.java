/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import planning.businesslogic.PlanningManager;
import java.util.Date;
import javax.swing.border.Border;
/**
 *
 * @author Erik
 */
public class AttendanceReportPanel extends JPanel {
    
    private JButton reportButton;
    private JTextField userName, passWord;
    JPanel contentPane;
    
    private final PlanningManager manager;
    
    //Creates new form PlanningUI
    public AttendanceReportPanel(){        
        manager = new PlanningManager();
        setLayout( new BorderLayout()); 
        Border border = BorderFactory.createEmptyBorder( 10, 10, 10, 10);
        setBorder( border );
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(4,2));
        userName = new JTextField("", 30);
        passWord = new JTextField("", 30);        
        
        //rij 1        
        contentPane.add(new JLabel("Gebruikers-ID"));
        contentPane.add(userName);
                
        //rij 2
        contentPane.add(new JLabel("Wachtwoord"));
        contentPane.add(passWord);
        
        //rij 3
        contentPane.add(new JLabel(" "));
        contentPane.add(new JLabel(" "));
        
        //rij 4
        contentPane.add(new JLabel(" "));
        reportButton = new JButton("Meld aanwezigheid");
        contentPane.add(reportButton);     
         
        reportButton.addActionListener ((ActionEvent e) -> {
            
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                Date datedate = new Date();
                String date = dateFormat.format(datedate)+ "";
                manager.reportAttendance(date);
                
                JOptionPane.showMessageDialog(null,"Aanwezigheid gemeld");  
                //dispose(); een manier vinden om et vensterje te laten verdwijnen als je je aanwezigheid hebt gemeld
        });
                        
        
        //set the size of the window
        setSize(600,200);
        
        //center screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        
        
        add(contentPane, BorderLayout.CENTER);
        
    }
    
    
}
