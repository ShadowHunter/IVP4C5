/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planning.presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import planning.businesslogic.PlanningManager;
/**
 *
 * @author Erik
 */
public class PlannerUI extends JFrame {
    
    private JTextField employeeNrField;
    private JPasswordField passwordField;
    
    private JButton loginButton;
    private JButton presentButton;
    private JButton planningButton;
    
    private JLabel nrLabel;
    private JLabel pwLabel;
    private JLabel picLabel;
    private JLabel disclaimLabel;
    
    private final PlanningManager manager;
    
    public PlannerUI() throws IOException{
        
        manager = new PlanningManager();
        //frame setup
        setTitle("Hoofdscherm planning");
        JPanel loginScreenPanel = new JPanel();
        loginScreenPanel.setLayout(new BorderLayout(5,5));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1000,800);
        
        //center screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        
        //setup of center area
        JPanel loginPanel = createLoginPanel();
        
        //setup of south area
        JPanel employeeOptionPanel = createEmployeeOptionPanel();
        
        add(loginPanel, BorderLayout.CENTER);
        add(employeeOptionPanel, BorderLayout.SOUTH);
        
        //add actionListeners
        loginButton.addActionListener((ActionEvent e) ->{
            
            if(!(employeeNrField.getText().isEmpty())){
                int employeeNr = Integer.parseInt(employeeNrField.getText());
                char[] input = passwordField.getPassword();
                String password = new String(input);
            
                boolean success = manager.plannerLogin(employeeNr, password);
                
                if(success){
                    JFrame planningFrame = new JFrame("Planning opties");
                    planningFrame.setSize(600,300);
                    planningFrame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
                    planningFrame.setContentPane(new PlanningUI());
                    planningFrame.setVisible(true);
                    planningFrame.setResizable(false);
            
                    //set screen
                    planningFrame.setLocation(670, 380);
            
                    dispose();
                }
                else{
                    JOptionPane.showMessageDialog(null,"Werknemersnummer of wachtwoord klopt niet");
                    employeeNrField.setText("");
                    passwordField.setText("");
                }
            }
            else{
                JOptionPane.showMessageDialog(null,"Vul een werknemersnummer in");
            }
        });
        
        /*presentButton.addActionListener ((ActionEvent e) -> {
            JFrame AttedanceCheckFrame = new JFrame("Aanwezigheid melden");
            AttedanceCheckFrame.setSize(600,200);
            AttedanceCheckFrame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
            AttedanceCheckFrame.setContentPane(new AttendanceReportPanel());
            AttedanceCheckFrame.setVisible(true);
            AttedanceCheckFrame.setResizable(false);

            
            //center screen
            AttedanceCheckFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
        }); */
        
        planningButton.addActionListener ((ActionEvent e) -> {  
            //make the dialog box
            JPanel panel = new JPanel(new BorderLayout(5, 5));

            JPanel label = new JPanel(new GridLayout(0, 1, 2, 2));
            label.add(new JLabel("Werknemersnummer", SwingConstants.RIGHT));
            label.add(new JLabel("Wachtwoord", SwingConstants.RIGHT));
            panel.add(label, BorderLayout.WEST);

            JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
            JTextField number = new JTextField();
            controls.add(number);
            JPasswordField passwordInput = new JPasswordField();
            controls.add(passwordInput);
            panel.add(controls, BorderLayout.CENTER);

            int reply = JOptionPane.showConfirmDialog(null, panel, "login", JOptionPane.OK_CANCEL_OPTION);
            
            //implement the action after ok press
            if(reply == JOptionPane.OK_OPTION){
                if(!(number.getText().isEmpty())){
                
                    int employeeNr = Integer.parseInt(number.getText());
                    String password = new String(passwordInput.getPassword());
                    
                    boolean success = manager.employeeLogin(employeeNr, password);
                    
                    if(success){
                        JFrame employeePlanningFrame = new JFrame("Weekplanning");
                        employeePlanningFrame.setSize(800,400);
                        employeePlanningFrame.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);
                        employeePlanningFrame.setContentPane(new EmployeePlanningPanel(employeeNr));
                        employeePlanningFrame.setVisible(true);
                        employeePlanningFrame.setResizable(false);
            
                        //center screen
                        employeePlanningFrame.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
                    }
                    else{
                    JOptionPane.showMessageDialog(null,"Werknemersnummer of wachtwoord klopt niet. \n Probeer opnieuw");
                    }
                }
                else{
                JOptionPane.showMessageDialog(null,"Vul een werknemersnummer in. \n Probeer opnieuw");
                }
            }
        });
    }
    
    public final JPanel createLoginPanel() throws IOException{
        
        JPanel loginPanel = new JPanel();
        loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.Y_AXIS));
        
        BufferedImage logo = ImageIO.read(new File("images\\Logo_HH.png"));
        picLabel = new JLabel(new ImageIcon(logo));
        picLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginPanel.add(picLabel);
        
        disclaimLabel = new JLabel("Deze login werkt alleen voor het planning personeel!");
        disclaimLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginPanel.add(disclaimLabel);
        
        loginPanel.add(new JLabel(" "));
        
        nrLabel = new JLabel("Werknemersnummer: ");
        nrLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginPanel.add(nrLabel); 
        
        employeeNrField = new JTextField();
        employeeNrField.setAlignmentX(Component.CENTER_ALIGNMENT);
        employeeNrField.setMaximumSize(new Dimension(50,25));
        loginPanel.add(employeeNrField);
        
        pwLabel = new JLabel("Wachtwoord: ");
        pwLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginPanel.add(pwLabel);
        
        passwordField = new JPasswordField();
        passwordField.setAlignmentX(Component.CENTER_ALIGNMENT);
        passwordField.setMaximumSize(new Dimension(200,25));
        loginPanel.add(passwordField);
        
        loginButton = new JButton("Login");
        loginButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginPanel.add(loginButton);
        
        return loginPanel;
    }
    
    public final JPanel createEmployeeOptionPanel(){
        
        JPanel employeeOptionPanel = new JPanel();
        employeeOptionPanel.setLayout(new BoxLayout(employeeOptionPanel, BoxLayout.X_AXIS));
        
        //employeeOptionPanel.add(new JLabel("Personeel: "));
        
        //presentButton = new JButton("Meld aanwezig");
        //employeeOptionPanel.add(presentButton);
        
        employeeOptionPanel.add(new JLabel("        Bekijk weekplanning: "));
        
        planningButton = new JButton("Planning");
        employeeOptionPanel.add(planningButton);
        
        return employeeOptionPanel;
        
    }
    
    
    
}
